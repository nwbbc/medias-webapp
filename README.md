# README #

Angular app that connects to ServerApi and displays all Medias data and allows editing of data for authorized users.

### Features ###

- Filter and search sermons 
- Share sermons via links (email/clipboard/sms)
- View download and publish stats 
- Publish discs (w/ labels) and USB flash drives
- Handles CD, DVD, mp3, mp4 in multiple formats/quality

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact